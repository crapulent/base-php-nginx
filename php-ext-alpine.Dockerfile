ARG SOURCE_IMAGE=php7.2-fpm-alpine3.9-nginx1.16
FROM ${SOURCE_IMAGE} as php

ARG PHPREDIS_VERSION
ENV PHPREDIS_VERSION ${PHPREDIS_VERSION:-5.0.2}

#we need icu-libs so intl can work
RUN apk add --no-cache icu-libs \
&& apk add --no-cache --virtual .build-deps-4-phpize m4 perl autoconf dpkg-dev dpkg libmagic file binutils gmp isl libgomp libatomic mpfr3 mpc1 gcc musl-dev libc-dev g++ make re2c \
&& apk add --no-cache --virtual .build-deps-4-php \
    libjpeg-turbo-dev libedit-dev libpng-dev icu-dev freetype-dev gmp-dev \
&& docker-php-ext-install \
    pdo_mysql \
    mysqli \
    json \
    readline \
    bcmath \
    gd \
    intl \
    opcache \
    gmp \
&& docker-php-ext-configure gd \
    --enable-gd-native-ttf \
    --with-freetype-dir=/usr/include/freetype2 \
    --with-jpeg-dir=/usr/include/ \
&& docker-php-ext-configure intl \
&& pecl install timezonedb && docker-php-ext-enable timezonedb \
&& curl -L -o /tmp/redis.tar.gz https://github.com/phpredis/phpredis/archive/$PHPREDIS_VERSION.tar.gz \
    && tar xfz /tmp/redis.tar.gz \
    && rm -r /tmp/redis.tar.gz \
    && mkdir -p /usr/src/php/ext \
    && mv phpredis-$PHPREDIS_VERSION /usr/src/php/ext/redis \
    && docker-php-ext-install redis \
    && apk info \
    && apk del .build-deps-4-phpize \
    && apk del .build-deps-4-php \
    && apk info && docker-php-source delete