ARG SOURCE_IMAGE=temp_image
FROM codeimpossible/dos2unix as prepare-entrypoint

COPY bin/docker-php-entrypoint .
RUN chmod +x docker-php-entrypoint && dos2unix docker-php-entrypoint

FROM ${SOURCE_IMAGE}

COPY --from=prepare-entrypoint docker-php-entrypoint /usr/local/bin/docker-php-entrypoint

COPY ./conf/php/ /usr/local/etc/php/conf.d/
COPY ./conf/nginx/nginx.conf /etc/nginx/nginx.conf
COPY ./conf/nginx/conf.d/ /etc/nginx/conf.d/
COPY ./conf/php-fpm/ /usr/local/etc/php-fpm.d/

HEALTHCHECK --interval=5s --timeout=3s \
  CMD curl -f http://localhost:81/ping || exit 1

ENV FPM_PM_MAX_CHILDREN=20 \
    FPM_PM_START_SERVERS=2 \
    FPM_PM_MIN_SPARE_SERVERS=1 \
    FPM_PM_MAX_SPARE_SERVERS=3

EXPOSE 80

ENTRYPOINT ["/usr/local/bin/docker-php-entrypoint"]
CMD ["/usr/bin/supervisord", "-n", "-c", "/etc/supervisor/conf.d/supervisord.conf"]