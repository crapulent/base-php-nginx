#!/bin/sh

set -e

if [ $# -eq 0 ]
  then
    exit 1
fi


if [ $# -eq 1 ]
  then
    IMG_NAME=${CI_REGISTRY_IMAGE}:$1;
  else
    IMG_NAME=$2;
fi

echo "pushing ${1} to ${IMG_NAME}"
docker tag "${1}" "${IMG_NAME}"
docker push "${IMG_NAME}"