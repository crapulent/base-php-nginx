#!/bin/sh

set -e

IMAGE_DEV=php${PHP_VERSION}-fpm-ext-nginx1.16-develop
docker build -f default.Dockerfile  -t temp_image --build-arg PHP_TAG="${PHP_VERSION}"-fpm .
docker build -f final.Dockerfile    -t "$IMAGE_DEV" .

IMAGE_SLIM_FULL=php${PHP_VERSION}-fpm-alpine${ALPINE_VERSION}-nginx1.16
docker build -f alpine.Dockerfile -t temp_image --build-arg PHP_TAG="${PHP_VERSION}"-fpm-alpine"${ALPINE_VERSION}" .
docker build -f final.Dockerfile  -t "$IMAGE_SLIM_FULL" .

IMAGE_EXT_FULL=php${PHP_VERSION}-fpm-ext-alpine${ALPINE_VERSION}-nginx1.17
docker build -f php-ext-alpine.Dockerfile -t temp_image --build-arg SOURCE_IMAGE="$IMAGE_SLIM_FULL" .
docker build -f final.Dockerfile          -t "$IMAGE_EXT_FULL" .

sh commands/push.sh "$IMAGE_DEV" "${CI_REGISTRY_IMAGE}"/"${CI_COMMIT_REF_SLUG}":"${CI_PIPELINE_IID}"-"${CI_COMMIT_SHORT_SHA}"-base-php-fpm-ext-nginx-develop
sh commands/push.sh "$IMAGE_EXT_FULL" "${CI_REGISTRY_IMAGE}"/"${CI_COMMIT_REF_SLUG}":"${CI_PIPELINE_IID}"-"${CI_COMMIT_SHORT_SHA}"-base-php-fpm-ext-alpine-nginx